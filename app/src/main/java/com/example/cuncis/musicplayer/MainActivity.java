package com.example.cuncis.musicplayer;

import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    private MediaPlayer mMediaPlayer;
    private TextView songName, songDuration;
    private SeekBar mSeekBar;
    private double timeStart = 0, finalTime = 0;
    private int forwardTime = 2000, backwardTime = 2000;
    private Handler durationHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        songName = findViewById(R.id.tv_song_name);
        songDuration = findViewById(R.id.tv_song_duration);
        mMediaPlayer = MediaPlayer.create(this, R.raw.grafittyfallsthemesong);
        mSeekBar = findViewById(R.id.seekBar);
        songName.setText("gravittyfals.mp3");
        mSeekBar.setMax((int)finalTime);
        mSeekBar.setClickable(false);
    }

    private Runnable updateSeekBarTime = new Runnable() {
        @Override
        public void run() {
            timeStart = mMediaPlayer.getCurrentPosition();
            mSeekBar.setProgress((int)timeStart);
            double timeRemaining = finalTime - timeStart;
            songDuration.setText(String.format("%d min, %d sec",
                    TimeUnit.MILLISECONDS.toMinutes((long) timeRemaining),
                    TimeUnit.MILLISECONDS.toSeconds((long) timeRemaining) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long)timeRemaining))));
            durationHandler.postDelayed(this, 100);
        }
    };

    public void backForward(View view) {
        if ((timeStart - backwardTime) > 0) {
            timeStart = timeStart - backwardTime;
            mMediaPlayer.seekTo((int)timeStart);
        }
    }

    public void pause(View view) {
        mMediaPlayer.pause();
    }

    public void play(View view) {
        mMediaPlayer.start();
        timeStart = mMediaPlayer.getCurrentPosition();
        mSeekBar.setProgress((int)timeStart);
        durationHandler.postDelayed(updateSeekBarTime, 100);
    }

    public void nextForward(View view) {
        if ((timeStart + forwardTime) <= finalTime) {
            timeStart = timeStart - backwardTime;
            mMediaPlayer.seekTo((int)timeStart);
        }
    }
}
